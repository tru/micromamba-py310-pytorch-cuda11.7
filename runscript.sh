#!/bin/bash
export MAMBA_ROOT_PREFIX=/opt/conda && \
eval "$(micromamba shell hook -s posix)"  && \
micromamba activate py310-pytorch-1.13.1  && \
"$@"
