 [![pipeline status](https://gitlab.pasteur.fr/tru/micromamba-py310-pytorch-cuda11.7/badges/main/pipeline.svg)](https://gitlab.pasteur.fr/tru/micromamba-py310-pytorch-cuda11.7/-/commits/main) 

 # micromamba based for python 3.10 and pytorch 1.13.1, pytorch-cuda=11.7
 Alternative to the regular command listed at https://pytorch.org/get-started/locally/
 
 `conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia`

 # Goals
 - smaller foot print for python3.10/pytorch gpu/cuda 11.7
 
 # removed on purpose from a plain installation (using `micromamba remove -y --force ....`)
 
 `conda create -n py310-pytorch python=3.10 && conda activate py310-pytorch && conda install pytorch  pytorch-cuda=11.7 -c pytorch -c nvidia -y`
- cuda-nsight
- cuda-nsight-compute
- nsight-compute
- cuda-demo-suite
-...
 # usage

docker:
```
docker run -ti docker://registry-gitlab.pasteur.fr/tru/micromamba-py310-pytorch-cuda11.7:main
```

singularity/apptainer:
```
singularity build micromamba-py310-pytorch-cuda11.7.sif oras://registry-gitlab.pasteur.fr/tru/micromamba-py310-pytorch-cuda11.7:latest
or
singularity build micromamba-py310-pytorch-cuda11.7.sif docker://registry-gitlab.pasteur.fr/tru/micromamba-py310-pytorch-cuda11.7:main
```
